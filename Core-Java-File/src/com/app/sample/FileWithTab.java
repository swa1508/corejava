package com.app.sample;

import java.io.File;
import java.io.FileWriter;

public class FileWithTab {

	public static void main(String[] args) {
	
		{  
			try  
			{  
				File file = new File("/home/swatantra/sample.txt");
				if (file.createNewFile()){
				    System.out.println("File is created!");
				    FileWriter writer = new FileWriter(file);
				    writer.write("Test1\t");
				    writer.write("Test2\t");
				    writer.write("Test3\t");
				    writer.write("Test4\t");
				    writer.write("Test5\t");
				    writer.write("\n");
				    
				    writer.write("Test1\t");
				    writer.write("Test2\t");
				    writer.write("Test3\t");
				    writer.write("Test4\t");
				    writer.write("Test5\t");
				    writer.write("\n");
				    
				    writer.write("Test1\t");
				    writer.write("Test2\t");
				    writer.write("Test3\t");
				    writer.write("Test4\t");
				    writer.write("Test5\t");
				    writer.write("\n");
				    writer.close();
				} else {
				    System.out.println("File already exists.");
				}
				
				
				
				/**Scanner sc=new Scanner(System.in);         //object of Scanner class  
				System.out.print("Enter the file name: ");  
				String name=sc.nextLine();              //variable name to store the file name  
				FileOutputStream fos=new FileOutputStream(name, true);  // true for append mode  
				System.out.print("Enter file content: ");         
				String str=sc.nextLine()+"\t";      //str stores the string which we have entered  
				byte[] b= str.getBytes();       //converts string into bytes  
				fos.write(b);           //writes bytes into file 
				
				System.out.print("Enter file content 2: ");         
				String str2=sc.nextLine()+"\t";      //str stores the string which we have entered  
				byte[] c= str2.getBytes();       //converts string into bytes  
				fos.write(c); 
				
				fos.close();            //close the file  
				System.out.println("file saved.");  **/
			}  
			catch(Exception e)  
			{  
				e.printStackTrace();          
			}  
		}  
	} 
}
