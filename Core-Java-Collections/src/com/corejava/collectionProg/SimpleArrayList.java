package com.corejava.collectionProg;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/*******************************************************************************************************************
 * 
 * Program 			:	Sample Array List and its different iterations .
 * 
 * Programmer		:	Swatantra Namdeo
 * 
 * JDk				:	11
 * 
 * Date				:	29-Aug-2020
 * 
 * 
 * ***********************************************************************************************************************/
public class SimpleArrayList {

	public static void main(String[] args) {
		System.out.println("===========================================================================");
		System.out.println("Hello Collections");
		System.out.println("===========================================================================");
		List<String> colors = new ArrayList<String>();
		colors.add("Red");
		colors.add("Blue");
		colors.add("Green");
		colors.add("Yellow");
		colors.add("Orange");

		/*** Type-1 : Iteration of Collection ( ForEach )******/
		
		for (String color : colors) {
			System.out.println("Color are : "+color);
		}
		System.out.println("===========================================================================");
				
		/*** Type-2 : Iteration of Collection ( Simple For loop)******/
		
		String[] colorsArr = new String[colors.size()];
		colorsArr  = colors.toArray(colorsArr);
		for(int i=0 ; i < colorsArr.length ; i++) {
			System.out.println("Color are : "+colorsArr[i]);
		}
		System.out.println("===========================================================================");
		/*** Type-3 : Iteration of Collection ( lambda Expression)******/
		
		colors.forEach(s -> System.out.println("Color are : "+s));
		System.out.println("===========================================================================");
		
		/*** Type-4 : Iteration of Collection (Using Iterator)******/
		Iterator<String> iterator = colors.iterator();
		while (iterator.hasNext()) {
			System.out.println("Color are : "+iterator.next());
			
		}
	}

}
